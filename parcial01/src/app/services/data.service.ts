import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Equipos} from '../models/equipos';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private dataCollection : AngularFirestoreCollection<Equipos>;
  private equipos : Observable<Equipos[]>;

  constructor(db: AngularFirestore) {
    this.dataCollection = db.collection<Equipos>('equipos');
    this.equipos = this.dataCollection.snapshotChanges().pipe(map(
      actions => {
        return actions.map( a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id, ...data}
        });
      }
    ))
  }
  getAllData() {
    return this.equipos;
  }
  getData(id:string) {
    return this.dataCollection.doc<Equipos>(id).valueChanges();
  }  
}
