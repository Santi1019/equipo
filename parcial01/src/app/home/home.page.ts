import { Component, OnInit } from '@angular/core';
import { Equipos } from '../models/equipos';
import { DataService } from '../services/data.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  equipos: Equipos[];
  constructor(private dataService: DataService) {}
  ngOnInit() {
    this.dataService.getAllData().subscribe(res => {
      console.log('Data', res);
      this.equipos = res;
    });
  }


}
