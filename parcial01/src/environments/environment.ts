// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyA3nlI5LgK67JYX3GCNjnZxASvPlVQTbNc",
    authDomain: "equipos-857ed.firebaseapp.com",
    databaseURL: "https://equipos-857ed.firebaseio.com",
    projectId: "equipos-857ed",
    storageBucket: "equipos-857ed.appspot.com",
    messagingSenderId: "143074622482",
    appId: "1:143074622482:web:f25f2a9f89d596db2b2ccb"
    }  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
